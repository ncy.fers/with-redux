import React, { useState } from 'react';
import api from '../../services/api';
import zlib from 'zlib';

export default function Search() {
    const [artist, setArtist] = useState('');
    const [previewUrl, setpreviewUrl] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();

        const response = await api.get('/search?term=' + artist);
        console.log(api.target);
        console.log(response.data);
    }

    return (
        <>
            <p> Informe seu artista </p>

            <form onSubmit={handleSubmit}>
                <label htmlFor="artist">Artista *</label>
                <input
                    type="artist"
                    id="artist"
                    placeholder="Seu artista favorito"
                    value={artist}
                    onChange={event => setArtist(event.target.value)}

                />
            </form>

            <div className="content">
                <video controls autoPlay name="media">
                    <source src={previewUrl} />
                </video>

            </div>
        </>)

}