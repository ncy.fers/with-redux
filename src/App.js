import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import Search from './components/search/search';

//import './sass/modules/styles.scss';

function App() {

  return (
    <div className="container">

      <h1 className="h1-fade">Welcome to My Songs</h1>

      <Search />
    </div>
  );
}

export default App;
